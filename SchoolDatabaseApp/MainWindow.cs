﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SchoolDatabaseApp
{
    public partial class MainWindow : Form
    {
        List<Tuple<string, string>> faculties = new List<Tuple<string, string>>();
        List<string> fieldsOfStudy = new List<string>();
        SqlConnection myConnection;

        public MainWindow()
        {
            myConnection = new SqlConnection(@"user id=Sokol-Komputer\Sokol;" +
                                            "password=;" +
                                            "server=SOKOL-KOMPUTER;" +
                                            "Trusted_Connection=yes;" +
                                            "database=SchoolDatabase;" +
                                            "connection timeout=30");

            myConnection.Open();

            InitializeComponent();
        }

        private void DeleteStudentsStudySemesters_Click(object sender, EventArgs e)
        {

        }

        private void btnShowCounsellorsInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnDeleteStudents_Click(object sender, EventArgs e)
        {

        }

        private void btnDeleteStudentsScholarships_Click(object sender, EventArgs e)
        {

        }

        private void btnDeleteStudySemesters_Click(object sender, EventArgs e)
        {

        }

        private void tabPage8_Click(object sender, EventArgs e)
        {

        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {

        }

        private void btnAddCounsellor_Click(object sender, EventArgs e)
        {

        }

        private void btnAddStudySemester_Click(object sender, EventArgs e)
        {
            try
            {
                var query = "SELECT * FROM utils.vFacultiesNames";
                var oCmd = new SqlCommand(query, myConnection);

                using (SqlDataReader reader = oCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        faculties.Add(new Tuple<string, string>(reader["FacultyId"].ToString(), reader["FacultyFullName"].ToString()));
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Blad " + exc.ToString());
            }

            cmbStudySemesterFaculty.Items.AddRange(faculties.Select(s => s.Item1 + " - " + s.Item2).ToArray());
        }

        private void cmbStudySemesterFaculty_SelectedIndexChanged(object sender, EventArgs e)
        {
            var index = cmbStudySemesterFaculty.SelectedIndex;

            if (index >= 0)
            {
                fieldsOfStudy.Clear();
                cmbStudySemesterFieldOfStudy.Items.Clear();

                try
                {
                    var query = string.Format("SELECT * FROM utils.vFieldsOfStudy WHERE FacultyId = '{0}'"
                                        , faculties[index].Item1);
                    //var query = "SELECT * FROM utils.vFieldsOfStudy WHERE FacultyId = '@FacultyId'";
                    var oCmd = new SqlCommand(query, myConnection);
                    //oCmd.Parameters.AddWithValue("@FacultyId", faculties[index].Item1);

                    using (SqlDataReader reader = oCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //var s = reader["FacultyId"].ToString() + " - " + reader["FacultyFullName"].ToString();
                            fieldsOfStudy.Add(reader["FieldOfStudyName"].ToString());
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Blad " + exc.ToString());
                }

                cmbStudySemesterFieldOfStudy.Items.AddRange(fieldsOfStudy.ToArray());
                cmbStudySemesterFieldOfStudy.SelectedIndex = 0;
            }
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            myConnection.Close();
        }
    }
}
