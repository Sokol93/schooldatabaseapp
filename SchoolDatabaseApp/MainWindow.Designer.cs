﻿namespace SchoolDatabaseApp
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlStudents = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.markerAddStudySemester = new System.Windows.Forms.Label();
            this.pnlStudentsAddStudySemester = new System.Windows.Forms.Panel();
            this.btnAddStudySemesterConfirm = new System.Windows.Forms.Button();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.cmbStudySemesterFieldOfStudy = new System.Windows.Forms.ComboBox();
            this.cmbStudySemesterFaculty = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.UpdateCoursesAddEmployees = new System.Windows.Forms.Button();
            this.btnAddCourses = new System.Windows.Forms.Button();
            this.btnAddStudentsScholarships = new System.Windows.Forms.Button();
            this.btnAddStudentsStudySemesters = new System.Windows.Forms.Button();
            this.btnAddStudySemester = new System.Windows.Forms.Button();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnDeleteStudySemesters = new System.Windows.Forms.Button();
            this.btnDeleteStudentsStudySemesters = new System.Windows.Forms.Button();
            this.btnDeleteStudentsScholarships = new System.Windows.Forms.Button();
            this.btnDeleteStudents = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnUpdateCoursesGrades = new System.Windows.Forms.Button();
            this.btnUpdateStudySemestersStatus = new System.Windows.Forms.Button();
            this.btnUpdateStudySemestersCounsellors = new System.Windows.Forms.Button();
            this.btnUpdateStudentsRepeatedSubjectsPayment = new System.Windows.Forms.Button();
            this.btnUpdateStudentsStudySemesterPayment = new System.Windows.Forms.Button();
            this.btnUpdateStudentsStudySemesterStatus = new System.Windows.Forms.Button();
            this.btnUpdateStudentsSurname = new System.Windows.Forms.Button();
            this.btnUpdateStudentsStatus = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btnShowStudentsCourses = new System.Windows.Forms.Button();
            this.btnShowStudySemesters = new System.Windows.Forms.Button();
            this.btnShowStudentsRepeatedSubjects = new System.Windows.Forms.Button();
            this.btnStudentsWhoHaventPaid = new System.Windows.Forms.Button();
            this.btnShowStudentsStudySemesters = new System.Windows.Forms.Button();
            this.btnShowStudentsScholarships = new System.Windows.Forms.Button();
            this.btnShowStudentFromFieldOfStudy = new System.Windows.Forms.Button();
            this.btnShowSpecificStudent = new System.Windows.Forms.Button();
            this.pnlEmployees = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.button14 = new System.Windows.Forms.Button();
            this.AddEmployeesSchoolSubjects = new System.Windows.Forms.Button();
            this.btnAddEmployeesDepartments = new System.Windows.Forms.Button();
            this.btnAddCounsellor = new System.Windows.Forms.Button();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.btnDeleteEmployee = new System.Windows.Forms.Button();
            this.btnDeleteCounsellor = new System.Windows.Forms.Button();
            this.btnDeleteEmployeesSchoolSubjects = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.btnUpdateEmployeesAcademicTitle = new System.Windows.Forms.Button();
            this.btnUpdateEmployeesStatus = new System.Windows.Forms.Button();
            this.btnUpdateEmployeesSurname = new System.Windows.Forms.Button();
            this.btnUpdateCounsellorEmail = new System.Windows.Forms.Button();
            this.btnUpdateCounsellorsPhone = new System.Windows.Forms.Button();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.btnShowSchoolSubjectsEmployees = new System.Windows.Forms.Button();
            this.btnShowEmployeesSchoolSubjects = new System.Windows.Forms.Button();
            this.btnShowDepartments = new System.Windows.Forms.Button();
            this.btnShowSpecificEmployee = new System.Windows.Forms.Button();
            this.btnShowCounsellorsInfo = new System.Windows.Forms.Button();
            this.btnShowEmployees = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.btnShowFieldOfStudySchoolSubjects = new System.Windows.Forms.Button();
            this.btnShowFacultyFieldsOfStudy = new System.Windows.Forms.Button();
            this.btnShowFaculties = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.btnArchivedFieldsOfStudy = new System.Windows.Forms.Button();
            this.btnShowArchivedEmployees = new System.Windows.Forms.Button();
            this.btnShowArchivedDepartments = new System.Windows.Forms.Button();
            this.btnShowArchivedCourses = new System.Windows.Forms.Button();
            this.ShowArchivedEmployeesDepartments = new System.Windows.Forms.Button();
            this.btnShowArchivedStudentsStudySemesters = new System.Windows.Forms.Button();
            this.btnShowArchivedStudySemesters = new System.Windows.Forms.Button();
            this.btnShowArchivedStudents = new System.Windows.Forms.Button();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.btnRestoreEmployee = new System.Windows.Forms.Button();
            this.btnRestoreStudent = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlStudents.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.pnlStudentsAddStudySemester.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.pnlEmployees.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlStudents
            // 
            this.pnlStudents.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.pnlStudents.Controls.Add(this.tabPage1);
            this.pnlStudents.Controls.Add(this.pnlEmployees);
            this.pnlStudents.Controls.Add(this.tabPage2);
            this.pnlStudents.Controls.Add(this.tabPage3);
            this.pnlStudents.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pnlStudents.Location = new System.Drawing.Point(2, 45);
            this.pnlStudents.Name = "pnlStudents";
            this.pnlStudents.SelectedIndex = 0;
            this.pnlStudents.Size = new System.Drawing.Size(1089, 484);
            this.pnlStudents.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tabControl1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1081, 448);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Students";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(-2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1086, 455);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.White;
            this.tabPage4.Controls.Add(this.markerAddStudySemester);
            this.tabPage4.Controls.Add(this.pnlStudentsAddStudySemester);
            this.tabPage4.Controls.Add(this.label2);
            this.tabPage4.Controls.Add(this.UpdateCoursesAddEmployees);
            this.tabPage4.Controls.Add(this.btnAddCourses);
            this.tabPage4.Controls.Add(this.btnAddStudentsScholarships);
            this.tabPage4.Controls.Add(this.btnAddStudentsStudySemesters);
            this.tabPage4.Controls.Add(this.btnAddStudySemester);
            this.tabPage4.Controls.Add(this.btnAddStudent);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1078, 422);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Add Data";
            // 
            // markerAddStudySemester
            // 
            this.markerAddStudySemester.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.markerAddStudySemester.Location = new System.Drawing.Point(347, 145);
            this.markerAddStudySemester.Name = "markerAddStudySemester";
            this.markerAddStudySemester.Size = new System.Drawing.Size(20, 2);
            this.markerAddStudySemester.TabIndex = 11;
            this.markerAddStudySemester.Text = "   ";
            // 
            // pnlStudentsAddStudySemester
            // 
            this.pnlStudentsAddStudySemester.Controls.Add(this.btnAddStudySemesterConfirm);
            this.pnlStudentsAddStudySemester.Controls.Add(this.comboBox4);
            this.pnlStudentsAddStudySemester.Controls.Add(this.comboBox3);
            this.pnlStudentsAddStudySemester.Controls.Add(this.cmbStudySemesterFieldOfStudy);
            this.pnlStudentsAddStudySemester.Controls.Add(this.cmbStudySemesterFaculty);
            this.pnlStudentsAddStudySemester.Controls.Add(this.label6);
            this.pnlStudentsAddStudySemester.Controls.Add(this.label5);
            this.pnlStudentsAddStudySemester.Controls.Add(this.label4);
            this.pnlStudentsAddStudySemester.Controls.Add(this.label3);
            this.pnlStudentsAddStudySemester.Location = new System.Drawing.Point(389, 11);
            this.pnlStudentsAddStudySemester.Name = "pnlStudentsAddStudySemester";
            this.pnlStudentsAddStudySemester.Size = new System.Drawing.Size(671, 400);
            this.pnlStudentsAddStudySemester.TabIndex = 7;
            // 
            // btnAddStudySemesterConfirm
            // 
            this.btnAddStudySemesterConfirm.AutoSize = true;
            this.btnAddStudySemesterConfirm.Location = new System.Drawing.Point(448, 252);
            this.btnAddStudySemesterConfirm.Name = "btnAddStudySemesterConfirm";
            this.btnAddStudySemesterConfirm.Size = new System.Drawing.Size(82, 32);
            this.btnAddStudySemesterConfirm.TabIndex = 10;
            this.btnAddStudySemesterConfirm.Text = "Confirm";
            this.btnAddStudySemesterConfirm.UseVisualStyleBackColor = true;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Jan Kowalski",
            "Agata Błażejewska",
            "Krzysztof Sokołowski"});
            this.comboBox4.Location = new System.Drawing.Point(240, 218);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(290, 28);
            this.comboBox4.TabIndex = 8;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Part time",
            "Full time",
            "Evening"});
            this.comboBox3.Location = new System.Drawing.Point(240, 184);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(290, 28);
            this.comboBox3.TabIndex = 7;
            // 
            // cmbStudySemesterFieldOfStudy
            // 
            this.cmbStudySemesterFieldOfStudy.FormattingEnabled = true;
            this.cmbStudySemesterFieldOfStudy.Items.AddRange(new object[] {
            "Informatics",
            "Telecommunications",
            "Automatics",
            "Robotics"});
            this.cmbStudySemesterFieldOfStudy.Location = new System.Drawing.Point(240, 150);
            this.cmbStudySemesterFieldOfStudy.Name = "cmbStudySemesterFieldOfStudy";
            this.cmbStudySemesterFieldOfStudy.Size = new System.Drawing.Size(290, 28);
            this.cmbStudySemesterFieldOfStudy.TabIndex = 6;
            // 
            // cmbStudySemesterFaculty
            // 
            this.cmbStudySemesterFaculty.FormattingEnabled = true;
            this.cmbStudySemesterFaculty.Location = new System.Drawing.Point(240, 116);
            this.cmbStudySemesterFaculty.Name = "cmbStudySemesterFaculty";
            this.cmbStudySemesterFaculty.Size = new System.Drawing.Size(290, 28);
            this.cmbStudySemesterFaculty.TabIndex = 5;
            this.cmbStudySemesterFaculty.SelectedIndexChanged += new System.EventHandler(this.cmbStudySemesterFaculty_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(140, 226);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Counsellor:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(140, 193);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Form of study:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(140, 160);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Field of Study:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(140, 127);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Faculty:";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(365, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(2, 400);
            this.label2.TabIndex = 6;
            // 
            // UpdateCoursesAddEmployees
            // 
            this.UpdateCoursesAddEmployees.Location = new System.Drawing.Point(6, 310);
            this.UpdateCoursesAddEmployees.Name = "UpdateCoursesAddEmployees";
            this.UpdateCoursesAddEmployees.Size = new System.Drawing.Size(337, 32);
            this.UpdateCoursesAddEmployees.TabIndex = 5;
            this.UpdateCoursesAddEmployees.Text = "Auto assign employees to courses";
            this.UpdateCoursesAddEmployees.UseVisualStyleBackColor = true;
            // 
            // btnAddCourses
            // 
            this.btnAddCourses.AutoSize = true;
            this.btnAddCourses.Location = new System.Drawing.Point(6, 264);
            this.btnAddCourses.Name = "btnAddCourses";
            this.btnAddCourses.Size = new System.Drawing.Size(337, 32);
            this.btnAddCourses.TabIndex = 4;
            this.btnAddCourses.Text = "Auto assign courses by study semester";
            this.btnAddCourses.UseVisualStyleBackColor = true;
            // 
            // btnAddStudentsScholarships
            // 
            this.btnAddStudentsScholarships.AutoSize = true;
            this.btnAddStudentsScholarships.Location = new System.Drawing.Point(6, 218);
            this.btnAddStudentsScholarships.Name = "btnAddStudentsScholarships";
            this.btnAddStudentsScholarships.Size = new System.Drawing.Size(337, 32);
            this.btnAddStudentsScholarships.TabIndex = 3;
            this.btnAddStudentsScholarships.Text = "Assign scholarship to student";
            this.btnAddStudentsScholarships.UseVisualStyleBackColor = true;
            // 
            // btnAddStudentsStudySemesters
            // 
            this.btnAddStudentsStudySemesters.AutoSize = true;
            this.btnAddStudentsStudySemesters.Location = new System.Drawing.Point(6, 172);
            this.btnAddStudentsStudySemesters.Name = "btnAddStudentsStudySemesters";
            this.btnAddStudentsStudySemesters.Size = new System.Drawing.Size(337, 32);
            this.btnAddStudentsStudySemesters.TabIndex = 2;
            this.btnAddStudentsStudySemesters.Text = "Assign student to study semester";
            this.btnAddStudentsStudySemesters.UseVisualStyleBackColor = true;
            // 
            // btnAddStudySemester
            // 
            this.btnAddStudySemester.AutoSize = true;
            this.btnAddStudySemester.Location = new System.Drawing.Point(6, 126);
            this.btnAddStudySemester.Name = "btnAddStudySemester";
            this.btnAddStudySemester.Size = new System.Drawing.Size(337, 32);
            this.btnAddStudySemester.TabIndex = 1;
            this.btnAddStudySemester.Text = "Study semester";
            this.btnAddStudySemester.UseVisualStyleBackColor = true;
            this.btnAddStudySemester.Click += new System.EventHandler(this.btnAddStudySemester_Click);
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.AutoSize = true;
            this.btnAddStudent.Location = new System.Drawing.Point(6, 80);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(337, 32);
            this.btnAddStudent.TabIndex = 0;
            this.btnAddStudent.Text = "Student";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnDeleteStudySemesters);
            this.tabPage5.Controls.Add(this.btnDeleteStudentsStudySemesters);
            this.tabPage5.Controls.Add(this.btnDeleteStudentsScholarships);
            this.tabPage5.Controls.Add(this.btnDeleteStudents);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1078, 422);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Remove Data";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnDeleteStudySemesters
            // 
            this.btnDeleteStudySemesters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteStudySemesters.Location = new System.Drawing.Point(6, 269);
            this.btnDeleteStudySemesters.Name = "btnDeleteStudySemesters";
            this.btnDeleteStudySemesters.Size = new System.Drawing.Size(316, 32);
            this.btnDeleteStudySemesters.TabIndex = 3;
            this.btnDeleteStudySemesters.Text = "Inactive study semesters";
            this.btnDeleteStudySemesters.UseVisualStyleBackColor = true;
            this.btnDeleteStudySemesters.Click += new System.EventHandler(this.btnDeleteStudySemesters_Click);
            // 
            // btnDeleteStudentsStudySemesters
            // 
            this.btnDeleteStudentsStudySemesters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteStudentsStudySemesters.Location = new System.Drawing.Point(6, 220);
            this.btnDeleteStudentsStudySemesters.Name = "btnDeleteStudentsStudySemesters";
            this.btnDeleteStudentsStudySemesters.Size = new System.Drawing.Size(316, 32);
            this.btnDeleteStudentsStudySemesters.TabIndex = 2;
            this.btnDeleteStudentsStudySemesters.Text = "Remove student from study semester";
            this.btnDeleteStudentsStudySemesters.UseVisualStyleBackColor = true;
            this.btnDeleteStudentsStudySemesters.Click += new System.EventHandler(this.DeleteStudentsStudySemesters_Click);
            // 
            // btnDeleteStudentsScholarships
            // 
            this.btnDeleteStudentsScholarships.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteStudentsScholarships.Location = new System.Drawing.Point(6, 171);
            this.btnDeleteStudentsScholarships.Name = "btnDeleteStudentsScholarships";
            this.btnDeleteStudentsScholarships.Size = new System.Drawing.Size(316, 32);
            this.btnDeleteStudentsScholarships.TabIndex = 1;
            this.btnDeleteStudentsScholarships.Text = "Student\'s scholarship";
            this.btnDeleteStudentsScholarships.UseVisualStyleBackColor = true;
            this.btnDeleteStudentsScholarships.Click += new System.EventHandler(this.btnDeleteStudentsScholarships_Click);
            // 
            // btnDeleteStudents
            // 
            this.btnDeleteStudents.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteStudents.Location = new System.Drawing.Point(6, 122);
            this.btnDeleteStudents.Name = "btnDeleteStudents";
            this.btnDeleteStudents.Size = new System.Drawing.Size(316, 32);
            this.btnDeleteStudents.TabIndex = 0;
            this.btnDeleteStudents.Text = "Inactive students";
            this.btnDeleteStudents.UseVisualStyleBackColor = true;
            this.btnDeleteStudents.Click += new System.EventHandler(this.btnDeleteStudents_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btnUpdateCoursesGrades);
            this.tabPage6.Controls.Add(this.btnUpdateStudySemestersStatus);
            this.tabPage6.Controls.Add(this.btnUpdateStudySemestersCounsellors);
            this.tabPage6.Controls.Add(this.btnUpdateStudentsRepeatedSubjectsPayment);
            this.tabPage6.Controls.Add(this.btnUpdateStudentsStudySemesterPayment);
            this.tabPage6.Controls.Add(this.btnUpdateStudentsStudySemesterStatus);
            this.tabPage6.Controls.Add(this.btnUpdateStudentsSurname);
            this.tabPage6.Controls.Add(this.btnUpdateStudentsStatus);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1078, 422);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Modify Data";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btnUpdateCoursesGrades
            // 
            this.btnUpdateCoursesGrades.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateCoursesGrades.Location = new System.Drawing.Point(6, 126);
            this.btnUpdateCoursesGrades.Name = "btnUpdateCoursesGrades";
            this.btnUpdateCoursesGrades.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateCoursesGrades.TabIndex = 7;
            this.btnUpdateCoursesGrades.Text = "Courses grades";
            this.btnUpdateCoursesGrades.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudySemestersStatus
            // 
            this.btnUpdateStudySemestersStatus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudySemestersStatus.Location = new System.Drawing.Point(6, 356);
            this.btnUpdateStudySemestersStatus.Name = "btnUpdateStudySemestersStatus";
            this.btnUpdateStudySemestersStatus.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudySemestersStatus.TabIndex = 6;
            this.btnUpdateStudySemestersStatus.Text = "Study semester status";
            this.btnUpdateStudySemestersStatus.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudySemestersCounsellors
            // 
            this.btnUpdateStudySemestersCounsellors.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudySemestersCounsellors.Location = new System.Drawing.Point(6, 310);
            this.btnUpdateStudySemestersCounsellors.Name = "btnUpdateStudySemestersCounsellors";
            this.btnUpdateStudySemestersCounsellors.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudySemestersCounsellors.TabIndex = 5;
            this.btnUpdateStudySemestersCounsellors.Text = "Sstudy semester counsellor";
            this.btnUpdateStudySemestersCounsellors.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudentsRepeatedSubjectsPayment
            // 
            this.btnUpdateStudentsRepeatedSubjectsPayment.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudentsRepeatedSubjectsPayment.Location = new System.Drawing.Point(6, 264);
            this.btnUpdateStudentsRepeatedSubjectsPayment.Name = "btnUpdateStudentsRepeatedSubjectsPayment";
            this.btnUpdateStudentsRepeatedSubjectsPayment.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudentsRepeatedSubjectsPayment.TabIndex = 4;
            this.btnUpdateStudentsRepeatedSubjectsPayment.Text = "Student\'s repeated subjects payment info";
            this.btnUpdateStudentsRepeatedSubjectsPayment.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudentsStudySemesterPayment
            // 
            this.btnUpdateStudentsStudySemesterPayment.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudentsStudySemesterPayment.Location = new System.Drawing.Point(6, 218);
            this.btnUpdateStudentsStudySemesterPayment.Name = "btnUpdateStudentsStudySemesterPayment";
            this.btnUpdateStudentsStudySemesterPayment.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudentsStudySemesterPayment.TabIndex = 3;
            this.btnUpdateStudentsStudySemesterPayment.Text = "Student\'s study semester payment info";
            this.btnUpdateStudentsStudySemesterPayment.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudentsStudySemesterStatus
            // 
            this.btnUpdateStudentsStudySemesterStatus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudentsStudySemesterStatus.Location = new System.Drawing.Point(6, 172);
            this.btnUpdateStudentsStudySemesterStatus.Name = "btnUpdateStudentsStudySemesterStatus";
            this.btnUpdateStudentsStudySemesterStatus.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudentsStudySemesterStatus.TabIndex = 2;
            this.btnUpdateStudentsStudySemesterStatus.Text = "Student\'s study semester status";
            this.btnUpdateStudentsStudySemesterStatus.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudentsSurname
            // 
            this.btnUpdateStudentsSurname.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudentsSurname.Location = new System.Drawing.Point(6, 80);
            this.btnUpdateStudentsSurname.Name = "btnUpdateStudentsSurname";
            this.btnUpdateStudentsSurname.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudentsSurname.TabIndex = 1;
            this.btnUpdateStudentsSurname.Text = "Student\'s surname";
            this.btnUpdateStudentsSurname.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStudentsStatus
            // 
            this.btnUpdateStudentsStatus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateStudentsStatus.Location = new System.Drawing.Point(6, 34);
            this.btnUpdateStudentsStatus.Name = "btnUpdateStudentsStatus";
            this.btnUpdateStudentsStatus.Size = new System.Drawing.Size(349, 32);
            this.btnUpdateStudentsStatus.TabIndex = 0;
            this.btnUpdateStudentsStatus.Text = "Student\'s status";
            this.btnUpdateStudentsStatus.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.btnShowStudentsCourses);
            this.tabPage7.Controls.Add(this.btnShowStudySemesters);
            this.tabPage7.Controls.Add(this.btnShowStudentsRepeatedSubjects);
            this.tabPage7.Controls.Add(this.btnStudentsWhoHaventPaid);
            this.tabPage7.Controls.Add(this.btnShowStudentsStudySemesters);
            this.tabPage7.Controls.Add(this.btnShowStudentsScholarships);
            this.tabPage7.Controls.Add(this.btnShowStudentFromFieldOfStudy);
            this.tabPage7.Controls.Add(this.btnShowSpecificStudent);
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1078, 422);
            this.tabPage7.TabIndex = 3;
            this.tabPage7.Text = "Show Data";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // btnShowStudentsCourses
            // 
            this.btnShowStudentsCourses.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowStudentsCourses.Location = new System.Drawing.Point(6, 356);
            this.btnShowStudentsCourses.Name = "btnShowStudentsCourses";
            this.btnShowStudentsCourses.Size = new System.Drawing.Size(330, 32);
            this.btnShowStudentsCourses.TabIndex = 7;
            this.btnShowStudentsCourses.Text = "All student\'s active courses";
            this.btnShowStudentsCourses.UseVisualStyleBackColor = true;
            // 
            // btnShowStudySemesters
            // 
            this.btnShowStudySemesters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowStudySemesters.Location = new System.Drawing.Point(6, 310);
            this.btnShowStudySemesters.Name = "btnShowStudySemesters";
            this.btnShowStudySemesters.Size = new System.Drawing.Size(330, 32);
            this.btnShowStudySemesters.TabIndex = 6;
            this.btnShowStudySemesters.Text = "Info about specified study semester";
            this.btnShowStudySemesters.UseVisualStyleBackColor = true;
            // 
            // btnShowStudentsRepeatedSubjects
            // 
            this.btnShowStudentsRepeatedSubjects.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowStudentsRepeatedSubjects.Location = new System.Drawing.Point(6, 264);
            this.btnShowStudentsRepeatedSubjects.Name = "btnShowStudentsRepeatedSubjects";
            this.btnShowStudentsRepeatedSubjects.Size = new System.Drawing.Size(330, 32);
            this.btnShowStudentsRepeatedSubjects.TabIndex = 5;
            this.btnShowStudentsRepeatedSubjects.Text = "Specified student\'s repeated courses";
            this.btnShowStudentsRepeatedSubjects.UseVisualStyleBackColor = true;
            // 
            // btnStudentsWhoHaventPaid
            // 
            this.btnStudentsWhoHaventPaid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnStudentsWhoHaventPaid.Location = new System.Drawing.Point(6, 218);
            this.btnStudentsWhoHaventPaid.Name = "btnStudentsWhoHaventPaid";
            this.btnStudentsWhoHaventPaid.Size = new System.Drawing.Size(330, 32);
            this.btnStudentsWhoHaventPaid.TabIndex = 4;
            this.btnStudentsWhoHaventPaid.Text = "Students who haven\'t paid all";
            this.btnStudentsWhoHaventPaid.UseVisualStyleBackColor = true;
            // 
            // btnShowStudentsStudySemesters
            // 
            this.btnShowStudentsStudySemesters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowStudentsStudySemesters.Location = new System.Drawing.Point(6, 126);
            this.btnShowStudentsStudySemesters.Name = "btnShowStudentsStudySemesters";
            this.btnShowStudentsStudySemesters.Size = new System.Drawing.Size(330, 32);
            this.btnShowStudentsStudySemesters.TabIndex = 3;
            this.btnShowStudentsStudySemesters.Text = "Specified student\'s study semesters";
            this.btnShowStudentsStudySemesters.UseVisualStyleBackColor = true;
            // 
            // btnShowStudentsScholarships
            // 
            this.btnShowStudentsScholarships.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowStudentsScholarships.Location = new System.Drawing.Point(6, 172);
            this.btnShowStudentsScholarships.Name = "btnShowStudentsScholarships";
            this.btnShowStudentsScholarships.Size = new System.Drawing.Size(330, 32);
            this.btnShowStudentsScholarships.TabIndex = 2;
            this.btnShowStudentsScholarships.Text = "Specified student\'s scholarships";
            this.btnShowStudentsScholarships.UseVisualStyleBackColor = true;
            // 
            // btnShowStudentFromFieldOfStudy
            // 
            this.btnShowStudentFromFieldOfStudy.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowStudentFromFieldOfStudy.Location = new System.Drawing.Point(6, 80);
            this.btnShowStudentFromFieldOfStudy.Name = "btnShowStudentFromFieldOfStudy";
            this.btnShowStudentFromFieldOfStudy.Size = new System.Drawing.Size(330, 32);
            this.btnShowStudentFromFieldOfStudy.TabIndex = 1;
            this.btnShowStudentFromFieldOfStudy.Text = "Students from the chosen field of study";
            this.btnShowStudentFromFieldOfStudy.UseVisualStyleBackColor = true;
            // 
            // btnShowSpecificStudent
            // 
            this.btnShowSpecificStudent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowSpecificStudent.Location = new System.Drawing.Point(6, 34);
            this.btnShowSpecificStudent.Name = "btnShowSpecificStudent";
            this.btnShowSpecificStudent.Size = new System.Drawing.Size(330, 32);
            this.btnShowSpecificStudent.TabIndex = 0;
            this.btnShowSpecificStudent.Text = "Specifiied student";
            this.btnShowSpecificStudent.UseVisualStyleBackColor = true;
            // 
            // pnlEmployees
            // 
            this.pnlEmployees.Controls.Add(this.tabControl2);
            this.pnlEmployees.Location = new System.Drawing.Point(4, 32);
            this.pnlEmployees.Name = "pnlEmployees";
            this.pnlEmployees.Padding = new System.Windows.Forms.Padding(3);
            this.pnlEmployees.Size = new System.Drawing.Size(1081, 448);
            this.pnlEmployees.TabIndex = 1;
            this.pnlEmployees.Text = "Employees";
            this.pnlEmployees.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1086, 452);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.button14);
            this.tabPage8.Controls.Add(this.AddEmployeesSchoolSubjects);
            this.tabPage8.Controls.Add(this.btnAddEmployeesDepartments);
            this.tabPage8.Controls.Add(this.btnAddCounsellor);
            this.tabPage8.Controls.Add(this.btnAddEmployee);
            this.tabPage8.Location = new System.Drawing.Point(4, 29);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(1078, 419);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Add Data";
            this.tabPage8.UseVisualStyleBackColor = true;
            this.tabPage8.Click += new System.EventHandler(this.tabPage8_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(961, 136);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 4;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // AddEmployeesSchoolSubjects
            // 
            this.AddEmployeesSchoolSubjects.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AddEmployeesSchoolSubjects.Location = new System.Drawing.Point(6, 271);
            this.AddEmployeesSchoolSubjects.Name = "AddEmployeesSchoolSubjects";
            this.AddEmployeesSchoolSubjects.Size = new System.Drawing.Size(415, 32);
            this.AddEmployeesSchoolSubjects.TabIndex = 3;
            this.AddEmployeesSchoolSubjects.Text = "Info about what school subjects can employee run";
            this.AddEmployeesSchoolSubjects.UseVisualStyleBackColor = true;
            // 
            // btnAddEmployeesDepartments
            // 
            this.btnAddEmployeesDepartments.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddEmployeesDepartments.Location = new System.Drawing.Point(6, 219);
            this.btnAddEmployeesDepartments.Name = "btnAddEmployeesDepartments";
            this.btnAddEmployeesDepartments.Size = new System.Drawing.Size(415, 32);
            this.btnAddEmployeesDepartments.TabIndex = 2;
            this.btnAddEmployeesDepartments.Text = "Info about which department employee belongs to";
            this.btnAddEmployeesDepartments.UseVisualStyleBackColor = true;
            // 
            // btnAddCounsellor
            // 
            this.btnAddCounsellor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddCounsellor.Location = new System.Drawing.Point(6, 167);
            this.btnAddCounsellor.Name = "btnAddCounsellor";
            this.btnAddCounsellor.Size = new System.Drawing.Size(415, 32);
            this.btnAddCounsellor.TabIndex = 1;
            this.btnAddCounsellor.Text = "Counsellor";
            this.btnAddCounsellor.UseVisualStyleBackColor = true;
            this.btnAddCounsellor.Click += new System.EventHandler(this.btnAddCounsellor_Click);
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddEmployee.Location = new System.Drawing.Point(6, 115);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(415, 32);
            this.btnAddEmployee.TabIndex = 0;
            this.btnAddEmployee.Text = "Employee";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.btnDeleteEmployee);
            this.tabPage9.Controls.Add(this.btnDeleteCounsellor);
            this.tabPage9.Controls.Add(this.btnDeleteEmployeesSchoolSubjects);
            this.tabPage9.Location = new System.Drawing.Point(4, 29);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(1078, 419);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Remove Data";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // btnDeleteEmployee
            // 
            this.btnDeleteEmployee.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteEmployee.Location = new System.Drawing.Point(3, 193);
            this.btnDeleteEmployee.Name = "btnDeleteEmployee";
            this.btnDeleteEmployee.Size = new System.Drawing.Size(384, 32);
            this.btnDeleteEmployee.TabIndex = 4;
            this.btnDeleteEmployee.Text = "Inactive employees";
            this.btnDeleteEmployee.UseVisualStyleBackColor = true;
            // 
            // btnDeleteCounsellor
            // 
            this.btnDeleteCounsellor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteCounsellor.Location = new System.Drawing.Point(3, 243);
            this.btnDeleteCounsellor.Name = "btnDeleteCounsellor";
            this.btnDeleteCounsellor.Size = new System.Drawing.Size(384, 32);
            this.btnDeleteCounsellor.TabIndex = 1;
            this.btnDeleteCounsellor.Text = "Counsellor";
            this.btnDeleteCounsellor.UseVisualStyleBackColor = true;
            // 
            // btnDeleteEmployeesSchoolSubjects
            // 
            this.btnDeleteEmployeesSchoolSubjects.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDeleteEmployeesSchoolSubjects.Location = new System.Drawing.Point(3, 143);
            this.btnDeleteEmployeesSchoolSubjects.Name = "btnDeleteEmployeesSchoolSubjects";
            this.btnDeleteEmployeesSchoolSubjects.Size = new System.Drawing.Size(384, 32);
            this.btnDeleteEmployeesSchoolSubjects.TabIndex = 0;
            this.btnDeleteEmployeesSchoolSubjects.Text = "Specific school subject that employee can run";
            this.btnDeleteEmployeesSchoolSubjects.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.btnUpdateEmployeesAcademicTitle);
            this.tabPage10.Controls.Add(this.btnUpdateEmployeesStatus);
            this.tabPage10.Controls.Add(this.btnUpdateEmployeesSurname);
            this.tabPage10.Controls.Add(this.btnUpdateCounsellorEmail);
            this.tabPage10.Controls.Add(this.btnUpdateCounsellorsPhone);
            this.tabPage10.Location = new System.Drawing.Point(4, 29);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(1078, 419);
            this.tabPage10.TabIndex = 2;
            this.tabPage10.Text = "Modify Data";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // btnUpdateEmployeesAcademicTitle
            // 
            this.btnUpdateEmployeesAcademicTitle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateEmployeesAcademicTitle.Location = new System.Drawing.Point(6, 288);
            this.btnUpdateEmployeesAcademicTitle.Name = "btnUpdateEmployeesAcademicTitle";
            this.btnUpdateEmployeesAcademicTitle.Size = new System.Drawing.Size(239, 32);
            this.btnUpdateEmployeesAcademicTitle.TabIndex = 4;
            this.btnUpdateEmployeesAcademicTitle.Text = "Employee\'s academic title";
            this.btnUpdateEmployeesAcademicTitle.UseVisualStyleBackColor = true;
            // 
            // btnUpdateEmployeesStatus
            // 
            this.btnUpdateEmployeesStatus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateEmployeesStatus.Location = new System.Drawing.Point(6, 240);
            this.btnUpdateEmployeesStatus.Name = "btnUpdateEmployeesStatus";
            this.btnUpdateEmployeesStatus.Size = new System.Drawing.Size(239, 32);
            this.btnUpdateEmployeesStatus.TabIndex = 3;
            this.btnUpdateEmployeesStatus.Text = "Employee\'s status";
            this.btnUpdateEmployeesStatus.UseVisualStyleBackColor = true;
            // 
            // btnUpdateEmployeesSurname
            // 
            this.btnUpdateEmployeesSurname.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateEmployeesSurname.Location = new System.Drawing.Point(6, 192);
            this.btnUpdateEmployeesSurname.Name = "btnUpdateEmployeesSurname";
            this.btnUpdateEmployeesSurname.Size = new System.Drawing.Size(239, 32);
            this.btnUpdateEmployeesSurname.TabIndex = 2;
            this.btnUpdateEmployeesSurname.Text = "Employee\'s surname";
            this.btnUpdateEmployeesSurname.UseVisualStyleBackColor = true;
            // 
            // btnUpdateCounsellorEmail
            // 
            this.btnUpdateCounsellorEmail.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateCounsellorEmail.Location = new System.Drawing.Point(6, 144);
            this.btnUpdateCounsellorEmail.Name = "btnUpdateCounsellorEmail";
            this.btnUpdateCounsellorEmail.Size = new System.Drawing.Size(239, 32);
            this.btnUpdateCounsellorEmail.TabIndex = 1;
            this.btnUpdateCounsellorEmail.Text = "Counsellor\'s e-mail";
            this.btnUpdateCounsellorEmail.UseVisualStyleBackColor = true;
            // 
            // btnUpdateCounsellorsPhone
            // 
            this.btnUpdateCounsellorsPhone.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateCounsellorsPhone.Location = new System.Drawing.Point(6, 96);
            this.btnUpdateCounsellorsPhone.Name = "btnUpdateCounsellorsPhone";
            this.btnUpdateCounsellorsPhone.Size = new System.Drawing.Size(239, 32);
            this.btnUpdateCounsellorsPhone.TabIndex = 0;
            this.btnUpdateCounsellorsPhone.Text = "Counsellor\'s phone number";
            this.btnUpdateCounsellorsPhone.UseVisualStyleBackColor = true;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.btnShowSchoolSubjectsEmployees);
            this.tabPage11.Controls.Add(this.btnShowEmployeesSchoolSubjects);
            this.tabPage11.Controls.Add(this.btnShowDepartments);
            this.tabPage11.Controls.Add(this.btnShowSpecificEmployee);
            this.tabPage11.Controls.Add(this.btnShowCounsellorsInfo);
            this.tabPage11.Controls.Add(this.btnShowEmployees);
            this.tabPage11.Location = new System.Drawing.Point(4, 29);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(1078, 419);
            this.tabPage11.TabIndex = 3;
            this.tabPage11.Text = "Show Data";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // btnShowSchoolSubjectsEmployees
            // 
            this.btnShowSchoolSubjectsEmployees.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowSchoolSubjectsEmployees.Location = new System.Drawing.Point(3, 315);
            this.btnShowSchoolSubjectsEmployees.Name = "btnShowSchoolSubjectsEmployees";
            this.btnShowSchoolSubjectsEmployees.Size = new System.Drawing.Size(406, 32);
            this.btnShowSchoolSubjectsEmployees.TabIndex = 5;
            this.btnShowSchoolSubjectsEmployees.Text = "Employees who can run specified school subject";
            this.btnShowSchoolSubjectsEmployees.UseVisualStyleBackColor = true;
            // 
            // btnShowEmployeesSchoolSubjects
            // 
            this.btnShowEmployeesSchoolSubjects.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowEmployeesSchoolSubjects.Location = new System.Drawing.Point(3, 266);
            this.btnShowEmployeesSchoolSubjects.Name = "btnShowEmployeesSchoolSubjects";
            this.btnShowEmployeesSchoolSubjects.Size = new System.Drawing.Size(406, 32);
            this.btnShowEmployeesSchoolSubjects.TabIndex = 4;
            this.btnShowEmployeesSchoolSubjects.Text = "School subjects that specified employee can run";
            this.btnShowEmployeesSchoolSubjects.UseVisualStyleBackColor = true;
            // 
            // btnShowDepartments
            // 
            this.btnShowDepartments.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowDepartments.Location = new System.Drawing.Point(6, 217);
            this.btnShowDepartments.Name = "btnShowDepartments";
            this.btnShowDepartments.Size = new System.Drawing.Size(406, 32);
            this.btnShowDepartments.TabIndex = 3;
            this.btnShowDepartments.Text = "All department\'s info";
            this.btnShowDepartments.UseVisualStyleBackColor = true;
            // 
            // btnShowSpecificEmployee
            // 
            this.btnShowSpecificEmployee.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowSpecificEmployee.Location = new System.Drawing.Point(6, 119);
            this.btnShowSpecificEmployee.Name = "btnShowSpecificEmployee";
            this.btnShowSpecificEmployee.Size = new System.Drawing.Size(406, 32);
            this.btnShowSpecificEmployee.TabIndex = 2;
            this.btnShowSpecificEmployee.Text = "Specific employee info";
            this.btnShowSpecificEmployee.UseVisualStyleBackColor = true;
            // 
            // btnShowCounsellorsInfo
            // 
            this.btnShowCounsellorsInfo.Location = new System.Drawing.Point(6, 168);
            this.btnShowCounsellorsInfo.Name = "btnShowCounsellorsInfo";
            this.btnShowCounsellorsInfo.Size = new System.Drawing.Size(406, 32);
            this.btnShowCounsellorsInfo.TabIndex = 1;
            this.btnShowCounsellorsInfo.Text = "Counsellors info";
            this.btnShowCounsellorsInfo.UseVisualStyleBackColor = true;
            this.btnShowCounsellorsInfo.Click += new System.EventHandler(this.btnShowCounsellorsInfo_Click);
            // 
            // btnShowEmployees
            // 
            this.btnShowEmployees.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnShowEmployees.Location = new System.Drawing.Point(6, 70);
            this.btnShowEmployees.Name = "btnShowEmployees";
            this.btnShowEmployees.Size = new System.Drawing.Size(406, 32);
            this.btnShowEmployees.TabIndex = 0;
            this.btnShowEmployees.Text = "All employees info";
            this.btnShowEmployees.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl3);
            this.tabPage2.Location = new System.Drawing.Point(4, 32);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1081, 448);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "School Structure";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage13);
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(1086, 452);
            this.tabControl3.TabIndex = 0;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.btnShowFieldOfStudySchoolSubjects);
            this.tabPage13.Controls.Add(this.btnShowFacultyFieldsOfStudy);
            this.tabPage13.Controls.Add(this.btnShowFaculties);
            this.tabPage13.Location = new System.Drawing.Point(4, 29);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(1078, 419);
            this.tabPage13.TabIndex = 1;
            this.tabPage13.Text = "Show Data";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // btnShowFieldOfStudySchoolSubjects
            // 
            this.btnShowFieldOfStudySchoolSubjects.Location = new System.Drawing.Point(6, 244);
            this.btnShowFieldOfStudySchoolSubjects.Name = "btnShowFieldOfStudySchoolSubjects";
            this.btnShowFieldOfStudySchoolSubjects.Size = new System.Drawing.Size(317, 32);
            this.btnShowFieldOfStudySchoolSubjects.TabIndex = 2;
            this.btnShowFieldOfStudySchoolSubjects.Text = "Field of study and it\'s school subjects";
            this.btnShowFieldOfStudySchoolSubjects.UseVisualStyleBackColor = true;
            // 
            // btnShowFacultyFieldsOfStudy
            // 
            this.btnShowFacultyFieldsOfStudy.Location = new System.Drawing.Point(6, 193);
            this.btnShowFacultyFieldsOfStudy.Name = "btnShowFacultyFieldsOfStudy";
            this.btnShowFacultyFieldsOfStudy.Size = new System.Drawing.Size(317, 32);
            this.btnShowFacultyFieldsOfStudy.TabIndex = 1;
            this.btnShowFacultyFieldsOfStudy.Text = "Specific faculty and it\'s fields of study";
            this.btnShowFacultyFieldsOfStudy.UseVisualStyleBackColor = true;
            // 
            // btnShowFaculties
            // 
            this.btnShowFaculties.Location = new System.Drawing.Point(6, 142);
            this.btnShowFaculties.Name = "btnShowFaculties";
            this.btnShowFaculties.Size = new System.Drawing.Size(317, 32);
            this.btnShowFaculties.TabIndex = 0;
            this.btnShowFaculties.Text = "All faculties info";
            this.btnShowFaculties.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tabControl4);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1081, 448);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Archived Data";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage12);
            this.tabControl4.Controls.Add(this.tabPage14);
            this.tabControl4.Location = new System.Drawing.Point(0, -2);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(1086, 452);
            this.tabControl4.TabIndex = 0;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.btnArchivedFieldsOfStudy);
            this.tabPage12.Controls.Add(this.btnShowArchivedEmployees);
            this.tabPage12.Controls.Add(this.btnShowArchivedDepartments);
            this.tabPage12.Controls.Add(this.btnShowArchivedCourses);
            this.tabPage12.Controls.Add(this.ShowArchivedEmployeesDepartments);
            this.tabPage12.Controls.Add(this.btnShowArchivedStudentsStudySemesters);
            this.tabPage12.Controls.Add(this.btnShowArchivedStudySemesters);
            this.tabPage12.Controls.Add(this.btnShowArchivedStudents);
            this.tabPage12.Location = new System.Drawing.Point(4, 29);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(1078, 419);
            this.tabPage12.TabIndex = 0;
            this.tabPage12.Text = "Show Data";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // btnArchivedFieldsOfStudy
            // 
            this.btnArchivedFieldsOfStudy.Location = new System.Drawing.Point(6, 372);
            this.btnArchivedFieldsOfStudy.Name = "btnArchivedFieldsOfStudy";
            this.btnArchivedFieldsOfStudy.Size = new System.Drawing.Size(379, 32);
            this.btnArchivedFieldsOfStudy.TabIndex = 7;
            this.btnArchivedFieldsOfStudy.Text = "Archived fields of study";
            this.btnArchivedFieldsOfStudy.UseVisualStyleBackColor = true;
            // 
            // btnShowArchivedEmployees
            // 
            this.btnShowArchivedEmployees.Location = new System.Drawing.Point(6, 219);
            this.btnShowArchivedEmployees.Name = "btnShowArchivedEmployees";
            this.btnShowArchivedEmployees.Size = new System.Drawing.Size(379, 32);
            this.btnShowArchivedEmployees.TabIndex = 6;
            this.btnShowArchivedEmployees.Text = "Archived employees";
            this.btnShowArchivedEmployees.UseVisualStyleBackColor = true;
            // 
            // btnShowArchivedDepartments
            // 
            this.btnShowArchivedDepartments.Location = new System.Drawing.Point(6, 270);
            this.btnShowArchivedDepartments.Name = "btnShowArchivedDepartments";
            this.btnShowArchivedDepartments.Size = new System.Drawing.Size(379, 32);
            this.btnShowArchivedDepartments.TabIndex = 5;
            this.btnShowArchivedDepartments.Text = "Archived departments";
            this.btnShowArchivedDepartments.UseVisualStyleBackColor = true;
            // 
            // btnShowArchivedCourses
            // 
            this.btnShowArchivedCourses.Location = new System.Drawing.Point(6, 66);
            this.btnShowArchivedCourses.Name = "btnShowArchivedCourses";
            this.btnShowArchivedCourses.Size = new System.Drawing.Size(379, 32);
            this.btnShowArchivedCourses.TabIndex = 4;
            this.btnShowArchivedCourses.Text = "Archived courses";
            this.btnShowArchivedCourses.UseVisualStyleBackColor = true;
            // 
            // ShowArchivedEmployeesDepartments
            // 
            this.ShowArchivedEmployeesDepartments.Location = new System.Drawing.Point(6, 321);
            this.ShowArchivedEmployeesDepartments.Name = "ShowArchivedEmployeesDepartments";
            this.ShowArchivedEmployeesDepartments.Size = new System.Drawing.Size(379, 32);
            this.ShowArchivedEmployeesDepartments.TabIndex = 3;
            this.ShowArchivedEmployeesDepartments.Text = "Archived employees and their departments";
            this.ShowArchivedEmployeesDepartments.UseVisualStyleBackColor = true;
            // 
            // btnShowArchivedStudentsStudySemesters
            // 
            this.btnShowArchivedStudentsStudySemesters.Location = new System.Drawing.Point(6, 168);
            this.btnShowArchivedStudentsStudySemesters.Name = "btnShowArchivedStudentsStudySemesters";
            this.btnShowArchivedStudentsStudySemesters.Size = new System.Drawing.Size(379, 32);
            this.btnShowArchivedStudentsStudySemesters.TabIndex = 2;
            this.btnShowArchivedStudentsStudySemesters.Text = "Specified student\'s archived study semesters";
            this.btnShowArchivedStudentsStudySemesters.UseVisualStyleBackColor = true;
            // 
            // btnShowArchivedStudySemesters
            // 
            this.btnShowArchivedStudySemesters.Location = new System.Drawing.Point(6, 117);
            this.btnShowArchivedStudySemesters.Name = "btnShowArchivedStudySemesters";
            this.btnShowArchivedStudySemesters.Size = new System.Drawing.Size(379, 32);
            this.btnShowArchivedStudySemesters.TabIndex = 1;
            this.btnShowArchivedStudySemesters.Text = "Archived study semesters";
            this.btnShowArchivedStudySemesters.UseVisualStyleBackColor = true;
            // 
            // btnShowArchivedStudents
            // 
            this.btnShowArchivedStudents.Location = new System.Drawing.Point(6, 15);
            this.btnShowArchivedStudents.Name = "btnShowArchivedStudents";
            this.btnShowArchivedStudents.Size = new System.Drawing.Size(379, 32);
            this.btnShowArchivedStudents.TabIndex = 0;
            this.btnShowArchivedStudents.Text = "Archived students";
            this.btnShowArchivedStudents.UseVisualStyleBackColor = true;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.btnRestoreEmployee);
            this.tabPage14.Controls.Add(this.btnRestoreStudent);
            this.tabPage14.Location = new System.Drawing.Point(4, 29);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(1078, 419);
            this.tabPage14.TabIndex = 1;
            this.tabPage14.Text = "Restore Data";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // btnRestoreEmployee
            // 
            this.btnRestoreEmployee.Location = new System.Drawing.Point(20, 69);
            this.btnRestoreEmployee.Name = "btnRestoreEmployee";
            this.btnRestoreEmployee.Size = new System.Drawing.Size(99, 32);
            this.btnRestoreEmployee.TabIndex = 1;
            this.btnRestoreEmployee.Text = "Employee";
            this.btnRestoreEmployee.UseVisualStyleBackColor = true;
            // 
            // btnRestoreStudent
            // 
            this.btnRestoreStudent.Location = new System.Drawing.Point(20, 15);
            this.btnRestoreStudent.Name = "btnRestoreStudent";
            this.btnRestoreStudent.Size = new System.Drawing.Size(99, 32);
            this.btnRestoreStudent.TabIndex = 0;
            this.btnRestoreStudent.Text = "Student";
            this.btnRestoreStudent.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(356, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to SchoolDatabase App!";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 529);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlStudents);
            this.Name = "MainWindow";
            this.Text = "SchoolDatabase";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.pnlStudents.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.pnlStudentsAddStudySemester.ResumeLayout(false);
            this.pnlStudentsAddStudySemester.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.pnlEmployees.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage13.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl pnlStudents;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage pnlEmployees;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.Button btnAddCourses;
        private System.Windows.Forms.Button btnAddStudentsScholarships;
        private System.Windows.Forms.Button btnAddStudentsStudySemesters;
        private System.Windows.Forms.Button btnAddStudySemester;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.Button btnDeleteStudySemesters;
        private System.Windows.Forms.Button btnDeleteStudentsStudySemesters;
        private System.Windows.Forms.Button btnDeleteStudentsScholarships;
        private System.Windows.Forms.Button btnDeleteStudents;
        private System.Windows.Forms.Button btnUpdateStudySemestersStatus;
        private System.Windows.Forms.Button btnUpdateStudySemestersCounsellors;
        private System.Windows.Forms.Button btnUpdateStudentsRepeatedSubjectsPayment;
        private System.Windows.Forms.Button btnUpdateStudentsStudySemesterPayment;
        private System.Windows.Forms.Button btnUpdateStudentsStudySemesterStatus;
        private System.Windows.Forms.Button btnUpdateStudentsSurname;
        private System.Windows.Forms.Button btnUpdateStudentsStatus;
        private System.Windows.Forms.Button UpdateCoursesAddEmployees;
        private System.Windows.Forms.Button btnUpdateCoursesGrades;
        private System.Windows.Forms.Button btnStudentsWhoHaventPaid;
        private System.Windows.Forms.Button btnShowStudentsStudySemesters;
        private System.Windows.Forms.Button btnShowStudentsScholarships;
        private System.Windows.Forms.Button btnShowStudentFromFieldOfStudy;
        private System.Windows.Forms.Button btnShowSpecificStudent;
        private System.Windows.Forms.Button btnShowStudentsCourses;
        private System.Windows.Forms.Button btnShowStudySemesters;
        private System.Windows.Forms.Button btnShowStudentsRepeatedSubjects;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button AddEmployeesSchoolSubjects;
        private System.Windows.Forms.Button btnAddEmployeesDepartments;
        private System.Windows.Forms.Button btnAddCounsellor;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Panel pnlStudentsAddStudySemester;
        private System.Windows.Forms.Button btnAddStudySemesterConfirm;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox cmbStudySemesterFieldOfStudy;
        private System.Windows.Forms.ComboBox cmbStudySemesterFaculty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label markerAddStudySemester;
        private System.Windows.Forms.Button btnDeleteEmployee;
        private System.Windows.Forms.Button btnDeleteCounsellor;
        private System.Windows.Forms.Button btnDeleteEmployeesSchoolSubjects;
        private System.Windows.Forms.Button btnUpdateEmployeesAcademicTitle;
        private System.Windows.Forms.Button btnUpdateEmployeesStatus;
        private System.Windows.Forms.Button btnUpdateEmployeesSurname;
        private System.Windows.Forms.Button btnUpdateCounsellorEmail;
        private System.Windows.Forms.Button btnUpdateCounsellorsPhone;
        private System.Windows.Forms.Button btnShowCounsellorsInfo;
        private System.Windows.Forms.Button btnShowEmployees;
        private System.Windows.Forms.Button btnShowEmployeesSchoolSubjects;
        private System.Windows.Forms.Button btnShowDepartments;
        private System.Windows.Forms.Button btnShowSpecificEmployee;
        private System.Windows.Forms.Button btnShowSchoolSubjectsEmployees;
        private System.Windows.Forms.Button btnShowFieldOfStudySchoolSubjects;
        private System.Windows.Forms.Button btnShowFacultyFieldsOfStudy;
        private System.Windows.Forms.Button btnShowFaculties;
        private System.Windows.Forms.Button btnArchivedFieldsOfStudy;
        private System.Windows.Forms.Button btnShowArchivedEmployees;
        private System.Windows.Forms.Button btnShowArchivedDepartments;
        private System.Windows.Forms.Button btnShowArchivedCourses;
        private System.Windows.Forms.Button ShowArchivedEmployeesDepartments;
        private System.Windows.Forms.Button btnShowArchivedStudentsStudySemesters;
        private System.Windows.Forms.Button btnShowArchivedStudySemesters;
        private System.Windows.Forms.Button btnShowArchivedStudents;
        private System.Windows.Forms.Button btnRestoreEmployee;
        private System.Windows.Forms.Button btnRestoreStudent;
    }
}

